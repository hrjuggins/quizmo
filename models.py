from app import db

class Quiz(db.Model):
    quiz_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    image = db.Column(db.String(100))
    description = db.Column(db.Text)

    def __init__(self, title, image, description):
        self.title = title
        self.image = image
        self.description = description


class Question(db.Model):
    question_id = db.Column(db.Integer, primary_key=True)
    quiz_id = db.Column(db.Integer)
    question = db.Column(db.Text)

    def __init__(self, quiz_id, question):
        self.quiz_id = quiz_id
        self.question = question


class Question_choices(db.Model):
    choice_id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer)
    quiz_id = db.Column(db.Integer)
    is_correct_choice = db.Column(db.Boolean)
    choice = db.Column(db.Text)

    def __init__(self, question_id, quiz_id, is_correct, choice):
        self.question_id = question_id
        self.quiz_id = quiz_id
        self.is_correct = is_correct
        self.choice = choice