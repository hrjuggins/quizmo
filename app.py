import os
from flask import Flask, url_for, redirect
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_flatpages import FlatPages

# from models import *

project_dir = os.path.abspath(os.path.dirname(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "quiz.db"))


app = Flask(__name__, static_url_path='/static')
app.config['SQLALCHEMY_DATABASE_URI'] = database_file
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class Quiz(db.Model):
    quiz_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    image = db.Column(db.String(100))
    description = db.Column(db.Text)

    def __init__(self, title, image, description):
        self.title = title
        self.image = image
        self.description = description


class Question(db.Model):
    question_id = db.Column(db.Integer, primary_key=True)
    quiz_id = db.Column(db.Integer)
    question = db.Column(db.Text)

    def __init__(self, quiz_id, question):
        self.quiz_id = quiz_id
        self.question = question


class Question_choices(db.Model):
    choice_id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer)
    quiz_id = db.Column(db.Integer)
    is_correct_choice = db.Column(db.Boolean)
    choice = db.Column(db.Text)

    def __init__(self, question_id, quiz_id, is_correct, choice):
        self.question_id = question_id
        self.quiz_id = quiz_id
        self.is_correct = is_correct
        self.choice = choice

# Upload Sample Quiz Data
#
# quiz1 = Quiz(title="History Quiz", image="")
# quiz1 = Quiz(
#     title="History Quiz",
#     question1="Who is the current President?",
#     answer="Trump",
#     incorrect1="asdf",
#     incorrect2="asdf",
#     incorrect3="asdf"
# )

# db.session.add(quiz1)
# db.session.commit()


@app.route("/", methods=['GET', 'POST'])
def main():
    quizzes = Quiz.query.all()
    return render_template("root.html", title = 'Projects', quizzes=quizzes)

@app.route("/quiz/<int:quiz_id>.html", methods=['GET', 'POST'])
def quizpage(quiz_id):
    quizzes = Quiz.query.all()
    print(quizzes[-1].quiz_id)
    currentquiz = Quiz.query.filter_by(quiz_id=quiz_id).first()
    currentquizquestions = Question.query.filter_by(quiz_id=quiz_id).all()
    currentquizchoices = Question_choices.query.filter_by(quiz_id=quiz_id).all()
    return render_template("quiz.html", quizzes=quizzes, currentquiz=currentquiz, \
    currentquizquestions=currentquizquestions, currentquizchoices=currentquizchoices)



if __name__ == "__main__":
    app.run(debug=True)
